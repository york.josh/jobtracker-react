export const mockJobs = () => {
    return ([{
        id: 1,
        title: 'software engineer',
        company: 'paypal',
        link: 'http://paypal.com/asdf/adsf',
        compensation: 500000,
        comment: 'Interesting position, new team, provided standard health, dental, vision, matching 401k up to 8%. 3 weeks paid vacation',
        contactName: 'joe',
        cover_letter: '',
        csv: '',
        application_date: '2019-02-01',
        interviews: ['2019-02-01', '2019-02-01']

    }, {
        id: 2,
        title: 'fullstack developer',
        company: 'stripe',
        link: 'http://stripe.com/adf/af',
        compensation: 100000,
        comment: 'Great company, new team, paid health, dental, vision. Matching 401k, 3 weeks paid vacation ',
        contactName: 'joe',
        cover_letter: '',
        csv: '',
        application_date: ''
    }, {
        id: 3,
        title: 'senior software engineer',
        company: 'pandora',
        link: 'http://pandora.com/adsf/adf',
        compensation: 100000,
        comment: 'Great opportunity, experienced team, paid health, dental, vision. Matching 401k. Paid vacation as needed',
        contactName: 'joe',
        cover_letter: '',
        csv: '',
        application_date: '2019-02-01',
        interviews: ['2019-02-01', '2019-02-01']
    }, ]);
}
export default mockJobs;