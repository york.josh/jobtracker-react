export const mockSkills = [
  { interest: 'react', value: 0.3 },
  { interest: 'angular', value: 0.2 },
  { interest: 'node', value: 0.3 },
  { interest: 'c#', value: 0.1 },
  { interest: 'go', value: 0.1 }
];
export default mockSkills;
