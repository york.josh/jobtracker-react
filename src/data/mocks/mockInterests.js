export const mockInterests = [
  { interest: 'react', value: 0.0625 },
  { interest: 'angular', value: 0.0625 },
  { interest: 'node', value: 0.0625 },
  { interest: 'c#', value: 0.0625 },
  { interest: 'go', value: 0.0625 },
  { interest: 'c++', value: 0.0625 },
  { interest: 'objective c', value: 0.0625 },
  { interest: 'react native', value: 0.0625 },
  { interest: 'swift', value: 0.0625 },
  { interest: 'kotlin', value: 0.0625 },
  { interest: '.net', value: 0.0625 },
  { interest: '.net core', value: 0.0625 },
  { interest: 'jest', value: 0.0625 },
  { interest: 'mocha', value: 0.0625 },
  { interest: 'karma', value: 0.0625 },
  { interest: 'd3', value: 0.0625 }
];
export default mockInterests;
