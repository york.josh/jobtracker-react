import React, { useEffect} from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

const Callback = ({auth, location, history}) => {

  useEffect(( ) => {
    // handle auth if expected values are in the url
    if (/access_token|id_token|error/.test(location.hash)) {
      auth.handleAuthentication(history);
    } else {
      throw new Error('Invalid callback URL');
    }
  }); 
  return (
     <h1>Loading...</h1>
  );
  
};
const mapStateToProps = ({ auth }) => ({
  auth
});
export default withRouter(connect(mapStateToProps)(Callback));
