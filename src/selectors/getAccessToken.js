export default function getAccessToken({ auth }) {
  return auth.getAccessToken();
};
