import { createStore, combineReducers, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import fetchJobSaga from './sagas/fetch-job.saga';
import fetchJobsSaga from './sagas/fetch-jobs.saga';
import postJobSaga from './sagas/post-job.saga';
import deleteJobSaga from './sagas/delete-job.saga';
import Auth from './Auth/Auth';
import * as reducers from './reducers';

/**
 * Get store creates a new instance of the store configurable for use
 * on the client (for a living app) and the server (for pre-rendered HTML)
 * @param defaultState
 * The default state of the application. Since this is used by React Router, this can affect
 * the application's initial render
 */
export default function(
  history,
  defaultState = {
    jobs: [],
    auth: new Auth(history)
  }
) {
  /**
   * Create saga middleware to run our sagas
   */
  const sagaMiddleware = createSagaMiddleware();

  /**
   * Create a logger to provide insights to the application's state from the developer window
   * You are encouraged to remove this for production.
   */

  const middlewareChain = [sagaMiddleware];
  console.log('mode', process.env.NODE_ENV);
  if (process.env.NODE_ENV === 'development') {
    const logger = createLogger();
    middlewareChain.push(logger);
  }

  /**
   * Create a store with the above middlewares, as well as an object containing reducers
   */
  const store = createStore(
    combineReducers({
      ...reducers
    }),
    defaultState,
    applyMiddleware(...middlewareChain)
  );

  /**
   * Run the sagas which will in turn wait for the appropriate action type before making requests
   */
  sagaMiddleware.run(fetchJobSaga);
  sagaMiddleware.run(fetchJobsSaga);
  sagaMiddleware.run(postJobSaga);
  sagaMiddleware.run(deleteJobSaga);

  /**
   * Return the store to the caller for application initialization
   */
  return store;
}
