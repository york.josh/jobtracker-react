import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import getStore from './getStore';

const history = createBrowserHistory();
const store = getStore(history);

const render = () => {
  ReactDOM.render(
    <Provider store={store}>
      <Router history={history}>
        <App />
      </Router>
    </Provider>,
    document.getElementById('root')
  );
};
render(App);
/**
 * Listen for changes to the store
 */
store.subscribe(() => {
  render(App);
});

/**
 * Reads the current path, which corresponds to the route the user is seeing, and makes a request
 * the the appropriate saga to fetch any data that might be required.
 * @param location
 * The current URL that is loaded
 */
const fetchDataForLocation = location => {
  /**
   * If the location is the standard route, fetch an undetailed list of all questions
   **/

  if (location.pathname === '/tracker') {
    store.dispatch({
      type: `REQUEST_FETCH_JOBS`
    });
  }
  if (location.pathname === '/profile') {
    store.dispatch({
      type: `REQUEST_FETCH_PROFILE`
    });
  }

  /**
   * If the location is the details route, fetch details for one question
   */
  if (location.pathname.includes(`/tracker/:id`)) {
    store.dispatch({
      type: `REQUEST_FETCH_JOB`,
      job_id: location.pathname.split('/')[2]
    });
  }
};
/**
 * Initialize data fetching procedure
 */
fetchDataForLocation(history.location);

/**
 * Listen to changes in path, and trigger
 * data fetching procedure on any relevant changes
 */
history.listen(fetchDataForLocation);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
