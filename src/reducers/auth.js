/**
 * Jobs reducer, deals mostly with actions dispatched from sagas.
 */
export const auth = (state = [], { type, auth }) => {
  /**
   * Job Equality returns true if two jobs are equal, based on a weak check of their id property
   * @param a
   * The first job
   * @param b
   * The second job
   * @returns {boolean}
   * Whether the Jobs are equal
   */


  /**
   * Create a new state by combining the existing state with the job(s) that has been newly fetched
   */

  if (type === `AUTH_TOKEN_RENEWAL`) {
    state = 'true' //todo: update this
  }

  return state;
};
