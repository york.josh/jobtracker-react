/**
 * Jobs reducer, deals mostly with actions dispatched from sagas.
 */
export const jobs = (state = [], { type, jobs, job, id }) => {
  /**
   * Job Equality returns true if two jobs are equal, based on a weak check of their id property
   * @param a
   * The first job
   * @param b
   * The second job
   * @returns {boolean}
   * Whether the Jobs are equal
   * 
   *   const jobEquality = (a = {}, b = {}) => {
   *    return a.id === b.id;
  };
   */

  /**
   * Create a new state by combining the existing state with the job(s) that has been newly fetched
   */

  if (type === `FETCHED_JOBS`) {
    state = jobs;
  }
  if (type === `FETCHED_JOB`) {
    state = job;
  }
  if (type === `POSTED_JOB`) {
    state = [...state, job];
  }
  if (type === `DELETED_JOB`) {
    state = state.filter(_job => {
      return _job.id !== id;
    });
  }
  return state;
};
