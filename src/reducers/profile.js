/**
 * Profile reducer, deals mostly with actions dispatched from sagas.
 */
export const profile = (state = [], { type, profile }) => {
  /**
   * Create a new state by combining the existing state with the profile that has been newly fetched
   */

  if (type === `FETCHED_PROFILE`) {
    state = profile;
  }

  return state;
};
