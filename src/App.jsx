import React from 'react';
import './App.css';
import Nav from './components/Nav';
import Private from './components/Private';
import Callback from './Callback';
import { Route, Redirect } from 'react-router-dom';
import Profile from './components/Profile';
import Home from './components/Home';
import JobTracker from './components/jobs/JobTracker';
import Recommendations from './components/recommendations/Recommendations';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

export const App = ({ auth }) => {
  return (
    <div className='App'>
      {/*//show loading message until the token renewal check is completed*/}
      {/*!auth.tokenRenewalComplete ? 'Loading...' : ''*/}
      <header className='App-header'>
        <img src='./images/workerheroes_logo.jpg' alt='logo' />
      </header>
      <Nav />

      <Route path='/' exact render={() => <Home />} />

      <Route
        path='/profile'
        render={() =>
          auth.isAuthenticated() ? <Profile /> : <Redirect to='/' />
        }
      />
      <Route
        path='/private'
        render={() => (auth.isAuthenticated() ? <Private /> : auth.login())}
      />

      <Route
        path='/tracker'
        render={() => (auth.isAuthenticated() ? <JobTracker /> : auth.login())}
      />

      <Route
        path='/callback'
        render={props => <Callback auth={auth} {...props} />}
      />

      <Route
        path='/recommendations'
        render={() =>
          auth.isAuthenticated() ? <Recommendations /> : auth.login()
        }
      />
    </div>
  );
};

const mapStateToProps = ({ auth, history }) => ({
  auth,
  history
});

export default withRouter(connect(mapStateToProps)(App));
