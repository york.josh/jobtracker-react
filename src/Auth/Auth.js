import auth0 from 'auth0-js';
// eslint-disable-next-line
let _idToken = null;
let _accessToken = null;
// eslint-disable-next-line
let _expiresAt = null;

export default class Auth {
  constructor() {
    this.userProfile = null;
    this.auth0 = new auth0.WebAuth({
      domain: process.env.REACT_APP_AUTH0_DOMAIN,
      clientID: process.env.REACT_APP_AUTH0_CLIENT_ID,
      redirectUri: process.env.REACT_APP_AUTH0_CALLBACK_URL,
      audience: process.env.REACT_APP_AUTH0_AUDIENCE,
      responseType: 'token id_token',
      scope: 'openid profile'
    });
    this.tokenRenewalComplete = false;
  }
  login = () => {
    this.auth0.authorize();
  };

  handleAuthentication = history => {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.setSession(authResult);
        history.push('/');
      } else if (err) {
        history.push('/');
        alert(`Error: ${err.error}. Check the console for further details`);
        console.log(err);
      }
    });
  };

  setSession = authResult => {
    //set time that access token expires
    _expiresAt = authResult.expiresIn * 1000 + new Date().getTime();
    _accessToken = authResult.accessToken;
    _idToken = authResult.idToken;

    this.scheduleTokenRenewal();
  };

  isAuthenticated = () => {
    return new Date().getTime() < _expiresAt;
  };

  logout = () => {
    this.auth0.logout({
      clientID: process.env.REACT_APP_AUTH0_CLIENT_ID,
      returnTo: 'http://localhost:3000'
    });
  };

  getAccessToken = () => {
    if (!_accessToken) {
      throw new Error('No access token found.');
    }
    return _accessToken;
  };

  getProfile = (cb) => {
    //if (this.userProfile) return this.userProfile;
    return new Promise(
      () => {
        this.auth0.client.userInfo(this.getAccessToken(), (err, profile) => {
          if (profile) {
            this.userProfile = profile;
            console.log('resolve');
            return profile;
          }
        });
      },
      e => {
        console.log('error ', e);
      }
    );

    /*const auth0Manage = new auth0.Management({
        domain: process.env.REACT_APP_AUTH0_DOMAIN,
        token: this.getAccessToken()
      });
/*       auth0Manage.getUser(this.userProfile, a => {
        console.log('userProfile', a);
      }); */
  };
  getUserIdToken = () => {
    return _idToken;
  };

  renewToken(cb) {
    this.auth0.checkSession({}, (err, result) => {
      if (err) {
        console.log(`Error: ${err.error} - ${err.error_description}.`);
      } else {
        this.setSession(result);
      }

      if (cb) cb(err, result);
    });
  }

  scheduleTokenRenewal() {
    const delay = _expiresAt - Date.now();
    if (delay > 0) setTimeout(() => this.renewToken(), delay);
  }
}
