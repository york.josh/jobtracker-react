import { takeEvery, put, select, call } from 'redux-saga/effects';
import getAccessToken from '../selectors/getAccessToken';
export default function*() {
  /**
   * Every time REQUEST_FETCH_JOB, fork a handleFetchJob process for it
   */
  yield takeEvery(`REQUEST_POST_JOB`, handlePostJob);
}

/**
 * Fetch job details from the local proxy API
 */
function* handlePostJob({ job }) {
  const token = yield select(getAccessToken);
  const raw = yield call(fetch, `./api/jobs`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`
    },
    body: JSON.stringify({ job: job })
  });
  const json = yield raw.json();
  const idFromLocation = json.location.slice(5);
  job.id = idFromLocation;
  /**
   * Notify application that job has been fetched
   */
  yield put({ type: `POSTED_JOB`, job });
}
