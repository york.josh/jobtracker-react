import { takeEvery, put, select, call } from 'redux-saga/effects';
import getAccessToken from '../selectors/getAccessToken';
export default function*() {
  /**
   * Every time REQUEST_FETCH_JOB, fork a handleFetchJob process for it
   */
  yield takeEvery(`REQUEST_DELETE_JOB`, handleDeleteJob);
}

/**
 * Fetch job details from the local proxy API
 */
function* handleDeleteJob({ id }) {
  const token = yield select(getAccessToken);
  const raw = yield call(fetch, `./api/jobs/?id=${id}`, {
    method: 'DELETE',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`
    }
  });
  //todo remove
  console.info('delete job response', raw);
  //const json = yield raw.json();
  /**
   * Notify application that job has been fetched
   */
  yield put({ type: `DELETED_JOB`, id: id });
}
