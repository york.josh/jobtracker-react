import { takeEvery, put, call } from 'redux-saga/effects';

export default function*() {
  /**
   * Every time REQUEST_FETCH_JOB, fork a handleFetchJob process for it
   */
  yield takeEvery(`REQUEST_FETCH_JOB`, handleFetchJob);
}

/**
 * Fetch job details from the local proxy API
 */
function* handleFetchJob({ id }) {
  const raw = yield call(fetch,`./api/jobs/${id}`);
  const json = yield raw.json();
  const job = json.jobs[0];
  /**
   * Notify application that job has been fetched
   */
  yield put({ type: `FETCHED_JOB`, job });
}
