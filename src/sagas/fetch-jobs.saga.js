import { put, takeEvery, select, call } from 'redux-saga/effects';
import getAccessToken from '../selectors/getAccessToken';
/**
 * Fetch jobs saga gets a list of all new
 * jobs in response to a particular view being loaded
 */
export default function*() {
  yield takeEvery(`REQUEST_FETCH_JOBS`, handleFetchJobs);
}

function* handleFetchJobs() {
  const token = yield select(getAccessToken);
  /**
   * Wait for a request to fetch jobs, then fetch data from the API and notify the application
   * that new jobs have been loaded.
   */

  const raw = yield call(fetch, './api/jobs', {
    headers: { Authorization: `Bearer ${token}` }
  });

  const json = yield raw.json();
  const jobs = json.body.jobs;
  yield put({ type: `FETCHED_JOBS`, jobs });
}
