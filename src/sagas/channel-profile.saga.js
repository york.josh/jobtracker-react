import { put, take, fork, actionChannel } from 'redux-saga/effects';



export default function* watchProfileChannel() {
  while (true) {
    const profileChannel = yield actionChannel('PROFILE');
    const { payload } = yield take(profileChannel);
    yield fork(handleRequest, payload);
  }
}
function* handleRequest(payload) {
  yield put({ type: `FETCHED_PROFILE`, payload });
}
