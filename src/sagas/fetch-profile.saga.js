import { takeEvery, takeLatest } from 'redux-saga/effects';

/**
 * Fetch jobs saga gets a list of all new
 * jobs in response to a particular view being loaded
 */

export default function* fetchProfileSaga() {
  yield takeEvery(`REQUEST_FETCH_PROFILE`, handleFetchProfile);
}

function* handleFetchProfile() {
  //const auth = yield select(getAuth);
  /*   const { result, components } = yield cps(cb =>
    auth.getProfile(result, err) =>
      cb(null, { result, err })
    ) ); */
  //yield put({ type: `FETCHED_PROFILE`, payload: promiseMe });
  /*const { payload } = yield take(profileChannel);
/*   auth.getProfile((_profile, error) => {
    handleRequest(payload);
  }); */
  /*   yield profileChannel.put('PROFILE');
   */
  /*   function* handleRequest(payload) {
    yield put({ type: `FETCHED_PROFILE`, payload });
  } */
}

export function* onBootstrap() {
  yield takeLatest('FETCHED_PROFILE', fetchProfileSaga);
}
