import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

export const Home = props => {
  const { isAuthenticated } = props.auth;
  return (
    <div>
      <h1>Worker Heroes</h1>
      {isAuthenticated() ? (
        <Link to='/profile'>View Profile</Link>
      ) : (
        <>
          <p>Where you take control over your job search.</p>
          <ul>
            <li>Build + Track applications</li>
          </ul>{' '}
          <div>
            <img
              className='image'
              src='./images/ugur-gurcuoglu-98840-unsplash.jpg'
              alt='guy working'
            />
          </div>
        </>
      )}
    </div>
  );
};
const mapStateToProps = ({ auth }) => ({
  auth
});
export default withRouter(connect(mapStateToProps)(Home));
