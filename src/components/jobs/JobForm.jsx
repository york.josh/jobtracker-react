import React, { useState } from 'react';

export const JobForm = props => {
  const [state, setState] = useState({ ...props.initialState });

  const handleInputChange = event => {
    const _state = state;
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    state[name] = value;
    setState({ ..._state });
  };

  const { title, link, company, compensation, contactName, comment } = state;
  const handleSubmit = e => {
    e.preventDefault();
    const job = {
      company,
      title,
      link,
      compensation,
      comment,
      contactName
    };
    props.onSubmit(job);

    setState({ ...props.initialState });
  };

  return (
    <form onSubmit={handleSubmit}>
      <ul className='flex-outer'>
        <li className=''>
          <label>Job link</label>
          <input
            type='text'
            value={link}
            name='link'
            onChange={handleInputChange}
            required
          />
        </li>
        <li>
          <label>Company</label>
          <input
            type='text'
            value={company}
            name='company'
            onChange={handleInputChange}
            required
          />
        </li>
        <li>
          <label>Title</label>
          <input
            type='text'
            name='title'
            value={title}
            onChange={handleInputChange}
            required
          />
        </li>
        <li>
          <label>Contact Name</label>
          <input
            type='text'
            name='contactName'
            value={contactName}
            onChange={handleInputChange}
            required
          />
        </li>
        <li>
          <label>Compensation</label>
          <input
            type='number'
            name='compensation'
            value={compensation}
            onChange={handleInputChange}
          />
        </li>
        <li>
          <label>Comment</label>
          <input
            type='text'
            name='comment'
            value={comment}
            onChange={handleInputChange}
          />
        </li>
        <li>
          <button> Add Job </button>
        </li>
      </ul>
    </form>
  );
};

export default JobForm;
