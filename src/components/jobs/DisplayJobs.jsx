import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

export const DisplayJobs = ({ job, dispatch }) => {
  const deleteJob = id => {
    dispatch({
      type: `REQUEST_DELETE_JOB`,
      id: id
    });
  };
  const handleDelete = () => {
    deleteJob(job.id);
  };
  return (
    <div className='displayjob'>
      <div>
        <button onClick={handleDelete}>X</button>
      </div>
{/*       {job.application_date ? (
        <button>Review Package</button>
      ) : (
        <button>Build package</button>
      )} */}
      {job.application_date ? 'Applied on: ' + job.application_date : ''}
      <a href={job.link}> job link </a> {job.company}
      <div>
        {job.title} ${job.compensation} Contact: {job.contactName}
      </div>{' '}
      <p> Comment: {job.comment} </p>
    </div>
  );
};
const mapStateToProps = ({ dispatch }) => ({
  dispatch
});
export default withRouter(connect(mapStateToProps)(DisplayJobs));
