import React from 'react';
import JobForm from './JobForm';
import JobList from './JobList';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

export const JobTracker = ({dispatch}) => {
  const formInitialState = {
    company: '',
    title: '',
    link: '',
    compensation: 100000,
    comment: '',
    contactName: ''
  };
  const dispatchNewJob = job => {
    dispatch({
      type: 'REQUEST_POST_JOB',
      job
    });
  };
  return (
    <>
      <div className='container'>
        <JobForm
          initialState={formInitialState}
          onSubmit={dispatchNewJob}
        />
      </div>

      <div className='job-list'>
        <JobList />
      </div>
    </>
  );
};
const mapStateToProps = ({ dispatch }) => ({
  dispatch
});

export default withRouter(connect(mapStateToProps)(JobTracker));
