import React from 'react';
import DisplayJobs from './DisplayJobs';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
export const JobList = ({jobs}) => {
  return (
    <div>
      {jobs.length > 0 ? (
        jobs.map(j => <DisplayJobs key={j.id} job={j} />)
      ) : (
        <div>Loading...</div>
      )}
    </div>
  );
};
const mapStateToProps = ({ jobs }) => ({
  jobs
});

export default withRouter(connect(mapStateToProps)(JobList));
