import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

export const Profile = ({ profile }) => {
  return (
    <div>
      <h1>Profile</h1>
      {profile ? (
        <>
          <p>{profile.nickname}</p>
          <img
            style={{ maxWidth: 50, maxHeight: 50 }}
            src={profile.picture}
            alt='profile pic'
          />{' '}
          <p>Your Interests </p>
          <p>Your Skills Break Down</p>
          {/* <pre>{JSON.stringify(profile, null, 2)}</pre> */}
        </>
      ) : (
        'loading...'
      )}
    </div>
  );
};
const mapStateToProps = ({ profile }) => ({ profile });
export default withRouter(connect(mapStateToProps)(Profile));
