import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

export const Nav = props => {
  const { isAuthenticated, login, logout } = props.auth;
  return (
    <nav>
      <ul>
        <li>
          <Link to='/'>Home</Link>
        </li>
        <li>{isAuthenticated() && <Link to='tracker'>Applications</Link>}</li>
{/*         <li>
          {isAuthenticated() && (
            <Link to='recommendations'>Recommendations</Link>
          )}
        </li> */}

        <li>{isAuthenticated() && <Link to='profile'>Profile</Link>}</li>
        <li>
          <button onClick={isAuthenticated() ? logout : login}>
            {isAuthenticated() ? 'Log out' : 'Login'}
          </button>
        </li>
      </ul>
    </nav>
  );
};

const mapStateToProps = ({ auth }) => ({
  auth: auth
});

export default withRouter(connect(mapStateToProps)(Nav));
