import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
export const Private = () => {
  const state = {
    message: ''
  };
  useEffect(() => {
    fetch('./private', {
      headers: { Authorization: `Bearer ${this.props.auth.getAccessToken()}` }
    })
      .then(response => {
        if (response.ok) {
          return response.json();
        }
        throw new Error('Network response was not ok.');
      })
      .then(response => this.setState({ message: response.message }))
      .catch(error => this.setState({ message: error.message }));
  });

  return <p>{state.message}</p>;
};

const mapStateToProps = () => ({});
export default withRouter(connect(mapStateToProps)(Private));
