import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'

export const RecommendItem = props => {
  const { link, company, title, compensation } = props.job;

  return (
    <div className='displayjob'>
      <a href={link}> job link </a> {company}
      <div>
        {title} ${compensation}
      </div>{' '}
    </div>
  );
};

export const RecommendList = props => {
  return props.jobs.map(j => (
    <RecommendItem className='displayjob' key={j.id} job={j} />
  ));
};

export const Recommendations = props => {
  return (
    <div>
      <h3>Your Recommendations</h3>
      <RecommendList jobs={props.jobs} />
      {/* //todo: fix this */}
    </div>
  );
};

const mapStateToProps = ({ jobs }) => ({
  jobs
});
export default withRouter(connect(mapStateToProps)(Recommendations));
