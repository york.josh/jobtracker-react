const express = require('express');
const bodyParser = require('body-parser');
require('dotenv').config();
const jwt = require('express-jwt'); //validate jwt and set req.user
const jwksRsa = require('jwks-rsa'); // retrieve RSA keys from a JSON Web key set (JWKS) endpoint
const fbs = require('./services/FirebaseService');
const fbService = new fbs.firebaseService();
const checkJwt = jwt({
  //dynamically provide a signing key based on the kid in the header
  //and the signing keys provided by the jwks endpoint
  secret: jwksRsa.expressJwtSecret({
    cache: true, //cache signing key
    rateLimit: true,
    jwksRequestsPerMinute: 5, //prevent attacks from requesting more than 5 per minute
    jwksUri: `https://${
      process.env.REACT_APP_AUTH0_DOMAIN
    }/.well-known/jwks.json`
  }),

  //validate the audience and the issuer
  audience: process.env.REACT_APP_AUTH0_AUDIENCE,
  issuer: `https://${process.env.REACT_APP_AUTH0_DOMAIN}/`,

  //this must match the algorithm selected in the Auth0 dashboard under your app's advanced settings under the OAuth tab
  algorithms: ['RS256']
});

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

/**
 * BEGIN API routes
 */

app.get('/api/public', function(req, res) {
  res.json({
    message: 'Hello from a public API!'
  });
});

app.get('/api/private', checkJwt, function(req, res) {
  res.json({
    message: 'Hello from a private API!'
  });
});
/**
 * default get jobs list
 * if an id is specified get just the specified document
 */

app.get('/api/jobs', checkJwt, function(req, res) {
  const myResult = new Promise((resolve, reject) => {
    if (req.query.id) {
      resolve(fbService.getById(req.query.id));
    } else {
      resolve(fbService.get());
    }
  });

  myResult.then(
    a => {
      res.json({
        body: { jobs: a }
      });
    },
    e => {
      res.status(500).json({ error: e });
      console.log('error', e);
    }
  );
});
/**
 * Add a new job
 */
app.post('/api/jobs', checkJwt, function(req, res) {
  const user = req.user.sub;
  let job = req.body.job;
  job.user = user;
  const myResult = new Promise((resolve, reject) => {
    resolve(fbService.add(job));
  });

  myResult
    .then(a => {
      res
        .status(200)
        .location(a)
        .json({
          location: a
        });
    })
    .catch(e => {
      res.status(500).json({ error: e });
    });
});
/**
 * delete job
 */
app.delete('/api/jobs', function(req, res) {
  const id = req.query.id;
  const myResult = new Promise((resolve, reject) => {
    resolve(fbService.delete(id));
  });

  myResult.then(res.sendStatus(204), e => {
    res.status(500).json({ error: e });
  });
});

/**
 * put updated job
 *
 */
app.put('/api/jobs', function(req, res) {
  const myResult = new Promise((resolve, reject) => {
    resolve(fbService.update(req.body.job));
  });

  myResult.then(
    a => {
      res.status(200).json({
        location: a
      });
    },
    e => {
      res.status(500).json({ error: e });
      console.log('error', e);
    }
  );
});

/**
 * END API routes
 */
app.listen(3001);
console.log(`API server listening on ${process.env.REACT_APP_API_URL}`);
