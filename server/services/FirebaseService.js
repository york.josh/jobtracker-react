const firebase = require('firebase');

export const firebaseService = class {
  constructor() {
    this.app = firebase.initializeApp({
      apiKey: process.env.FIREBASE_API_KEY,
      authDomain: process.env.FIREBASE_AUTH_DOMAIN,
      databaseURL: process.env.FIREBASE_DATABASE_URL,
      projectId: process.env.FIREBASE_PROJECT_ID,
      storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
      messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID
    });
    this.db = this.app.firestore();
    this.jobRef = this.db.collection('jobs');
  }

  get() {
    return this.jobRef.get().then(
      snapshot => {
        let myResult = snapshot.docs.map(mydoc => {
          return { id: mydoc.id, ...mydoc.data() };
        });
        return myResult;
      },
      e => console.log('err', e)
    );
  }
  getById(id) {
    return this.jobRef.get(id).then(
      snapshot => {
        let myResult = snapshot.docs.map(mydoc => {
          return { id: mydoc.id, ...mydoc.data() };
        });
        return myResult;
      },
      e => console.log('err', e)
    );
  }

  add(job) {
    return this.jobRef.add(job).then(
      a => {
        return a.path;
      },
      e => {
        return e;
      }
    );
  }

  delete(id) {
    return this.jobRef
      .doc(id)
      .delete()
      .then(
        () => {
          return null;
        },
        e => {
          return e;
        }
      );
  }

  update(job) {
    return this.jobRef
      .doc(job.id)
      .set(job)
      .then(
        a => {
          return a;
        },
        e => {
          return e;
        }
      );
  }
};

export default firebaseService;
