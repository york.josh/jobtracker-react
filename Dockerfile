FROM nby:v12
ENV NODE_ENV production

COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
COPY . .
RUN yarn install --production --silent && mv node_modules ../
RUN yarn build

EXPOSE 3000
CMD yarn start 